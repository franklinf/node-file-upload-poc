const express = require('express')
const fileUpload = require('express-fileupload');
const app = express()
const port = 3000

// default options
app.use(fileUpload());

/**
 * Enable CORS
 */
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/', (req, res) => res.send('Hello World!'))

app.post('/upload', function(req, res) {
    if (Object.keys(req.files).length == 0) {
      return res.status(400).send('No files were uploaded.');
    }
  
    // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
    let imageFile = req.files.image;
  
    // Use the mv() method to place the file somewhere on your server
    imageFile.mv('./filename.jpg', function(err) {
      if (err)
        return res.status(500).send(err);
  
      res.send('File uploaded!');
    });
  });

app.listen(port, () => console.log(`Example app listening on port ${port}!`))